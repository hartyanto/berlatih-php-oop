<?php


class Animal {
    public  $legs,
            $cold_blooded,
            $name;

    public function __construct($name = '', $legs = 2, $cold_blooded = 'false'){
        $this->name = $name;
        $this->legs = $legs;
        $this->cold_blooded = $cold_blooded;
    }

    public function get_name(){
        echo $this->name;
    }

    public function get_legs(){
        echo $this->legs;
    }

    public function get_cold_blooded(){
        echo $this->cold_blooded;
    }
}