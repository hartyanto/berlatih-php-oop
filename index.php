<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');


$sheep = new Animal("shaun");
$sheep->get_name();
echo "<br>";
$sheep->get_legs();
echo "<br>";
$sheep->get_cold_blooded();
echo "<hr>";

$kodok = new Frog("buduk");
$kodok->get_name();
echo "<br>";
$kodok->get_legs();
echo "<br>";
$kodok->get_cold_blooded();
echo "<br>";
$kodok->jump();
echo "<hr>";

echo "<br>";
$sungokong = new Ape("kera sakti");
$sungokong->get_name();
echo "<br>";
$sungokong->get_legs();
echo "<br>";
$sungokong->get_cold_blooded();
echo "<br>";
$sungokong->yell();

// var_dump($sheep);
// var_dump($kodok);